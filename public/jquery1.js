$(document).ready(function() {
    //  $.ajax({
    //  dataType: 'json',
    //    url: 'https://jsonplaceholder'
    // })
    // });


    var getUsersCall = {
        dataType: 'json',
        url: 'https://jsonplaceholder.typicode.com/posts',
        dataType: 'json',
        method: 'GET'

    };

    function printUsers(users) {
        console.log(users);
        $('#dataTable').dataTable({
            data: users,
            columns: [{
                    title: 'ID',
                    data: 'id'
                },
                {
                    title: 'Title',
                    data: 'title'
                },
                {
                    title: 'Body',
                    data: 'body'
                }
            ],
        });
    }

    function logearMensaje(mensaje) {
        console.log('error', mensaje);
    }

    function notificarUsuario() {
        alert('The request is complete!');
    }

    $.ajax(getUsersCall)
        .done(printUsers)
        .fail(logearMensaje)
        .always(notificarUsuario)
});
